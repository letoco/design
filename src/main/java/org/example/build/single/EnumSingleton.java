package org.example.build.single;

/**
 * 类描述：使用枚举类型
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public enum EnumSingleton {
    INSTANCE;

    public void print(){
        System.out.println("this.hashCode" + this.hashCode());
    }
}
