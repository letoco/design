package org.example.build.single;

/**
 * 类描述：懒加载单例
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class LazySingleton {
    // 避免指令重排
    private volatile static LazySingleton instance;

    private LazySingleton(){
        if (instance != null) {
            // 避免重复创建对象
            throw new UnsupportedOperationException("单例模式不支持创建多个对象！");
        }
    }

    public static LazySingleton getInstance() {
        if (instance == null) {
            // 避免
            synchronized (LazySingleton.class) {
                if (instance == null) {
                    // 如果可能发生指令重排可能导致先赋值后创建对象
                    // 导致获得的为空对象
                    instance = new LazySingleton();
                }
            }
        }
        return instance;
    }
}
