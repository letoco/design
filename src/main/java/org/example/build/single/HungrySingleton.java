package org.example.build.single;

/**
 * 类描述：饥饿式单例模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class HungrySingleton {
    private static final HungrySingleton INSTANCE = new HungrySingleton();

    private HungrySingleton() {
        if (INSTANCE != null) {
            throw new UnsupportedOperationException("单例模式不允许创建多个示例");
        }
    }

    public static HungrySingleton getInstance() {
        return INSTANCE;
    }
}
