package org.example.build.prototype;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.*;

/**
 * 类描述：深拷贝
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class DeepCopyDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException, CloneNotSupportedException {
        // 通过序列化和反序列实现深拷贝
        Demo demo = new Demo().setUser(new User().setName("李四").setAge(18));
        Demo deepCopy = DeepCopyDemo.deepCopy(demo);
        deepCopy.getUser().setAge(19);

        // 浅拷贝
        Demo clone = demo.clone();
        clone.getUser().setName("王五");

        System.out.println(demo);
        System.out.println(deepCopy);
        System.out.println(clone);
    }

    /**
     * 深拷贝工具类，通过序列化和非序列实现，因此，需要深拷贝的对象必须实现Serializable接口，
     * 且对象中如果存在对象引用，该对象引用所指向的对象也必须实现 Serializable，否则会抛出异常
     *
     * @param source 要深拷贝的对象
     * @param <T>    泛型
     * @return 拷贝的对象
     */
    public static <T> T deepCopy(T source) throws IOException, ClassNotFoundException {
        if (source instanceof Serializable) {
            // ByteArrayOutStream指向内存中的一个字节数组，用于保存序列化后的对象
            // 该流不需要进行关闭，因为当不需要使用时，GC会自动回收该对象
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            // 创建对象输出流
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            // 写入对象
            oos.writeObject(source);
            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (T) ois.readObject();
        }
        throw new NotSerializableException("被拷贝的对象必须实现Serializable接口！");
    }

    @Data
    @Accessors(chain = true)
    static class User implements Serializable,Cloneable {
        private static final long serialVersionUID = 3760742309246179789L;
        private String name;
        private Integer age;

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }

    @Data
    @Accessors(chain = true)
    static class Demo implements Serializable, Cloneable {
        private static final long serialVersionUID = -1497612712921169083L;
        private User user;

        @Override
        protected Demo clone() throws CloneNotSupportedException {
            return (Demo) super.clone();
        }
    }
}
