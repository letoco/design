package org.example.build.prototype;

/**
 * 类描述：原型模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class PrototypeDemo {
    public static void main(String[] args) {
        // 创建原型对象
        ConcretePrototype prototype = new ConcretePrototype("原型");
        ConcretePrototype clone = prototype.clone();
        System.out.println(clone);
    }

    // 抽象原型
    interface IPrototype<T> {
        T clone();
    }

    // 具体原型
    static class ConcretePrototype implements IPrototype<ConcretePrototype> {
        private String desc;

        public ConcretePrototype(String desc) {
            this.desc = desc;
        }

        @Override
        public ConcretePrototype clone() {
            return new ConcretePrototype(this.desc);
        }

        @Override
        public String toString() {
            return "ConcretePrototype{" +
                    "desc='" + desc + '\'' +
                    '}';
        }
    }
}
