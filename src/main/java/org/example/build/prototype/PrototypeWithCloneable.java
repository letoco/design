package org.example.build.prototype;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.*;

/**
 * 类描述：使用Object.clone进行复制
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class PrototypeWithCloneable {

    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {
        ConcretePrototype prototype = new ConcretePrototype("测试");
        ConcretePrototype clone = prototype.clone();
        System.out.println(clone);

        // 深拷贝测试
        ConcretePrototype concretePrototype = prototype.deepCopy();
        System.out.println(concretePrototype);
    }

    static class ConcretePrototype implements Cloneable, Serializable {

        private static final long serialVersionUID = 592690891175499131L;
        private String desc;

        public ConcretePrototype(String desc) {
            this.desc = desc;
        }

        @Override
        protected ConcretePrototype clone() throws CloneNotSupportedException {
            ConcretePrototype cloneType = null;

            // super.clone() 方法直接从堆内存中以二进制流的方式进行复制，重新分配一个内存块，因此其效率很高
            // super.clone() 基于内存复制，属于浅拷贝
            cloneType = (ConcretePrototype) super.clone();
            return cloneType;
        }

        public ConcretePrototype deepCopy() {
            try {
                // 将对象流输出到字节数组
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(bos);

                // 将当前对象写入流
                oos.writeObject(this);

                // 从字节数组中读取对象流
                ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
                ObjectInputStream ois = new ObjectInputStream(bis);

                return (ConcretePrototype) ois.readObject();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public String toString() {
            return "ConcretePrototype{" +
                    "desc='" + desc + '\'' +
                    '}';
        }
    }
}
