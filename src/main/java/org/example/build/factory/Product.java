package org.example.build.factory;

/**
 * 类描述：产品类接口
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public interface Product {
    void say();
}
