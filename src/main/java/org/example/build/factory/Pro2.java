package org.example.build.factory;

/**
 * 类描述：产品2
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class Pro2 implements Product{
    @Override
    public void say() {
        System.out.println("Pro2");
    }
}
