package org.example.build.factory;

/**
 * 类描述：枚举实现简单工厂方式2
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public enum SimpleFactoryEnum1 {
    PRO1() {
        @Override
        public Product create() {
            return new Pro1();
        }
    },
    PRO2() {
        // 由匿名内部类进行创建
        @Override
        public Product create() {
            return new Pro2();
        }
    };

    // 抽象方法，由子类实现
    public abstract Product create();
}
