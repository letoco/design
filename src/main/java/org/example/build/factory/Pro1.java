package org.example.build.factory;

/**
 * 类描述：产品1
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class Pro1 implements Product{
    @Override
    public void say(){
        System.out.println("产品1");
    }
}
