package org.example.build.factory;

/**
 * 类描述：枚举类型实现简单工厂
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public enum SimpleFactoryEnum {
    PRO1,
    PRO2;

    public Product create() {
        switch (this) {
            case PRO1:
                return new Pro1();
            case PRO2:
                return new Pro2();
            default:
                throw new UnsupportedOperationException("非法操作！");
        }
    }
}
