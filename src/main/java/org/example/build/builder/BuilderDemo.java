package org.example.build.builder;

import lombok.Data;

/**
 * 类描述：建造者模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class BuilderDemo {
    public static void main(String[] args) {
        // 测试
        Product product = Product.builder()
                .setName("李四").setPrice(18).setSize(20)
                .build();
        System.out.println(product);
    }

    @Data
    static class Product {
        private String name;
        private Integer price;
        private Integer size;

        public static Builder builder() {
            // 返回一个构建器
            return new Builder();
        }

        private Product(String name, Integer price, Integer size) {
            this.name = name;
            this.price = price;
            this.size = size;
        }

        static class Builder {
            private String name;
            private Integer price;
            private Integer size;

            public Builder setName(String name) {
                this.name = name;
                return this;
            }

            public Builder setPrice(Integer price) {
                this.price = price;
                return this;
            }

            public Builder setSize(Integer size) {
                this.size = size;
                return this;
            }

            public Product build() {
                return new Product(name, price, size);
            }
        }
    }
}
