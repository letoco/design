package org.example;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Supplier;

/**
 * 类描述：模板方法模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/28
 **/
public class TemplateMethodDemo {
    public static void main(String[] args) {
        AbstractClass clazz = new SubClass();
        clazz.operation();
    }

    abstract static class AbstractClass {
        public void operation(){
            // open
            System.out.println("pre ...");
            System.out.println("step1 ...");
            System.out.println("step2 ...");

            templateMethod();

            // close
            System.out.println("close ...");
        }

        protected abstract void templateMethod();
    }

    static class SubClass extends AbstractClass{

        @Override
        protected void templateMethod() {
            System.out.println("SubClass ...");
        }
    }
}
