package org.example;

/**
 * 类描述：策略模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class StrategyDemo {

    public static void main(String[] args) {
        Context context = new Context();
        // 这里使用Lambda来使用策略接口
        context.setStrategy(Math::min).handle(18,20);
        context.setStrategy(Math::max).handle(18,20);
    }

    // 策略接口
    static interface Strategy {
        int calc(int a, int b);
    }

    static class Context {
        private Strategy strategy;

        public Strategy getStrategy() {
            return strategy;
        }

        public Context setStrategy(Strategy strategy) {
            this.strategy = strategy;
            return this;
        }

        public void handle(int a, int b) {
            System.out.println("其他相关处理");
            // 调用策略方法
            int calc = strategy.calc(a, b);
            System.out.println(calc);
        }
    }
}
