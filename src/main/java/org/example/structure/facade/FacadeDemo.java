package org.example.structure.facade;

/**
 * 类描述：门面模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/30
 **/
public class FacadeDemo {
    public static void main(String[] args) {
        // 门面模式一般是用来整合系统的复杂API，对外提供统一的功能
        Facade facade = new Facade();

        facade.doA();
        facade.doB();
        facade.doC();
    }

    static class Facade {
        private SubSystemA systemA = new SubSystemA();
        private SubSystemB systemB = new SubSystemB();
        private SubSystemC systemC = new SubSystemC();

        public void doA() {
            systemA.doA();
        }

        public void doB(){
            systemB.doB();
        }

        public void doC(){
            systemC.doC();
        }
    }

    static class SubSystemA{
        // 子系统A
        public void doA() {
            System.out.println("A系统提供的功能");
        }
    }

    static class SubSystemB{
        public void doB() {
            System.out.println("B系统提供的功能");
        }
    }

    static class SubSystemC{
        public void doC() {
            System.out.println("C系统提供的功能");
        }
    }
}
