package org.example.structure.proxy;

import com.sun.org.apache.xpath.internal.axes.OneStepIterator;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 类描述：Cglib动态代理
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class CglibProxyDemo {
    public static void main(String[] args) {
        // 先创建目标对象
        User user = new User();

        // 创建增强类对象
        Enhancer enhancer = new Enhancer();
        // 设置需要增强的类
        enhancer.setSuperclass(User.class);
        // 设置回调函数
        enhancer.setCallback(new MethodInterceptor() {
            /**
             * 支持两种方式访问原对象的方法：
             *   1.反射方式：method.invoke()
             *   2.快速访问原始类的代理类：methodProxy.invoke()【推荐】
             * @param o 代表Cglib生成的动态代理类
             * @param method 代理类中被拦截的方法
             * @param objects 拦截方法的参数
             * @param methodProxy 快速访问目标类的原始方法，即代理类父类的方法
             * @return
             * @throws Throwable
             */
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("前置方法");
//                Object invoke = methodProxy.invoke(user, objects);
                Object invoke = method.invoke(user, objects);
                System.out.println("后置方法");

                System.out.println(o.getClass().getName());
                System.out.println(methodProxy.getClass().getName());
                return invoke;
            }
        });

        // 创建增强类
        User o = (User) enhancer.create();


        String s = o.get("世界", 2);
        System.out.println(s);
    }

    static class User {
        public String get(String s, Integer integer) {
            System.out.println("目标方法执行!");
            return s + integer;
        }
    }
}
