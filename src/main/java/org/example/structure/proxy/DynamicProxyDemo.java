package org.example.structure.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 类描述：JDK 动态代理测试
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class DynamicProxyDemo {

    public static void main(String[] args) {
        // 1.创建目标对象
        Traffic car = new Car();
        // 2.获取动态代理对象
        Traffic proxy = (Traffic) new JdkProxy(car).getProxy();
        // 3.使用代理对象执行相关方法
        proxy.run();
    }

    interface Traffic {
        void run();
    }

    static class Car implements Traffic {
        @Override
        public void run() {
            System.out.println("汽车在公路上行驶！");
        }
    }

    static class JdkProxy implements InvocationHandler {
        // 目标对象
        private Object target;

        public JdkProxy(Object target) {
            this.target = target;
        }

        /**
         * 用于书写额外功能
         *
         * @param proxy  代表代理对象
         * @param method 额外功能，所增加给的那个原始方法
         * @param args   原始方法的参数
         * @return
         * @throws Throwable
         */
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            before();
            Object result = method.invoke(target, args);
            after();
            return result;
        }

        private void after() {
            System.out.println("前置业务操作...");
        }

        private void before() {
            System.out.println("前置业务操作...");
        }

        public Object getProxy() {
            return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                    target.getClass().getInterfaces(), this);
        }
    }
}
