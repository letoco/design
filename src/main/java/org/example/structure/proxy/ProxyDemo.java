package org.example.structure.proxy;

/**
 * 类描述：代理模式测试，静态代理
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/29
 **/
public class ProxyDemo {

    public static void main(String[] args) {
        // 1.创建目标对象
        Animal cat = new Cat();
        // 2.创建代理对象
        Animal proxy = new CatProxy(cat);
        // 3.执行增强方法
        proxy.eat();
    }

    // 定义一类行为
    interface Animal {
        void eat();
    }

    // 创建目标类
    static class Cat implements Animal {
        @Override
        public void eat() {
            System.out.println("Cat eating...");
        }
    }

    // 代理增强类
    static class CatProxy implements Animal {
        // 目标类的引用
        Animal animal;

        public CatProxy(Animal animal) {
            this.animal = animal;
        }

        @Override
        public void eat() {
            before();
            animal.eat();
            after();
        }

        private void after() {
            System.out.println("方法执行后：" + System.currentTimeMillis());
        }

        private void before() {
            System.out.println("方法执行前：" + System.currentTimeMillis());
        }
    }
}
