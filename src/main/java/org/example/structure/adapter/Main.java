package org.example.structure.adapter;

/**
 * 类描述：
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/30
 **/
public class Main {
    public static void main(String[] args) {
        DC5 adapter = new PowerAdapter();
        adapter.output();
    }
}
