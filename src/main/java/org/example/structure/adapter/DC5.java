package org.example.structure.adapter;

/**
 * 类描述：DC5,5V直流电
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/30
 **/
public interface DC5 {
    int output();
}
