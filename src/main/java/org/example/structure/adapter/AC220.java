package org.example.structure.adapter;

/**
 * 类描述：220V电压输出
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/30
 **/
public class AC220 {
    public int outputAC220() {
        int output = 220;
        System.out.println("输出电压：" + output);
        return output;
    }
}
