package org.example.structure.adapter;

/**
 * 类描述：电源适配器
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/30
 **/
public class PowerAdapter extends AC220 implements DC5 {
    @Override
    public int output() {
        int adapterInput = super.outputAC220();
        int adapterOutput = adapterInput / 44;
        System.out.println("使用Adapter将输入" + adapterInput + "转换成" + adapterOutput);
        return adapterOutput;
    }
}
