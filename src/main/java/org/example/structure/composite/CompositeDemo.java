package org.example.structure.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * 类描述：组合模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/30
 **/
public class CompositeDemo {
    public static void main(String[] args) {
        // 创建根结点
        Component root = new Composite("root");
        // 创建一个树枝节点
        Component branchA = new Composite("---branchA");
        Component branchB = new Composite("------branchB");
        // 创建叶子节点
        Component leafA = new Leaf("------leafA");
        Component leafB = new Leaf("---------leafB");
        Component leafC = new Leaf("---leafC");

        // 节点组装
        root.addChild(branchA);
        root.addChild(leafC);

        branchA.addChild(leafA);
        branchA.addChild(branchB);

        branchB.addChild(leafB);

        String result = root.operation();
        System.out.println(result);
    }

    // 抽象节点
    static abstract class Component {
        protected String name;

        public Component(String name) {
            this.name = name;
        }

        public abstract String operation();

        public boolean addChild(Component component) {
            throw new UnsupportedOperationException("addChild not supported");
        }

        public boolean removeChild(Component component) {
            throw new UnsupportedOperationException("removeChild not supported");
        }

        public Component getChild(int index) {
            throw new UnsupportedOperationException("getChild not supported");
        }
    }

    // 树枝节点
    static class Composite extends Component {
        private List<Component> components;

        public Composite(String name) {
            super(name);
            this.components = new ArrayList<>();
        }

        @Override
        public String operation() {
            StringBuilder builder = new StringBuilder(this.name);
            for (Component component : this.components) {
                builder.append("\n");
                builder.append(component.operation());
            }
            return builder.toString();
        }

        @Override
        public boolean addChild(Component component) {
            return components.add(component);
        }

        @Override
        public boolean removeChild(Component component) {
            return components.remove(component);
        }

        @Override
        public Component getChild(int index) {
            return components.get(index);
        }
    }

    // 叶子节点
    static class Leaf extends Component {
        public Leaf(String name) {
            super(name);
        }

        @Override
        public String operation() {
            return this.name;
        }
    }
}
