package org.example;

import java.util.ArrayList;
import java.util.List;

/**
 * 类描述：观察者模式
 * <br>
 *
 * @author ZS
 * @version 1.0
 * @date 2020/12/28
 **/
public class ObserverDemo {

    public static void main(String[] args) {
        Subject subject = new Subject();

        Task1 task1 = new Task1();
        Task2 task2 = new Task2();

        // 注册任务
        subject.addObserver(task1);
        subject.addObserver(task2);

        // 发布消息
        subject.notifyObserver("世界");
    }

    // 观察者接口
    interface Observer {
        void update(Object object);
    }

    // 主题角色
    static class Subject {
        // 容器
        private List<Observer> container = new ArrayList<>();

        // 注册
        public boolean addObserver(Observer observer) {
            return container.add(observer);
        }

        // 取消注册
        public boolean removeObserver(Observer observer) {
            return container.remove(observer);
        }

        // 事件发生变化，通知观察者
        public void notifyObserver(Object object) {
            container.forEach(observer -> {
                // 通知观察者
                observer.update(object);
            });
        }
    }

    static class Task1 implements Observer {
        @Override
        public void update(Object object) {
            System.out.println("task1接收到了通知：" + object);
        }
    }
    static class Task2 implements Observer {
        @Override
        public void update(Object object) {
            System.out.println("task2接收到了通知：" + object);
        }
    }
}
